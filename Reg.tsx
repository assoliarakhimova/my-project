import React from "react";
import { Controller, useForm } from 'react-hook-form';
import { FormEvent } from 'react';
import './Reg.module.css';
import { FormInput } from "./forminput";
import styles from './Reg.module.css'
import { Link } from 'react-router-dom';

export  type SignUpForm = {
  username: string;
  email:string;
  password: string;
  confirmPassword: string;
};

type VerifyForm ={
    otp:string;
}


const defaultValues: SignUpForm  = {
    username: "",
    email: "",
    password:"", 
    confirmPassword:""
  };
  
type Props = {
    initialValues?: SignUpForm;
    isLoading: boolean;
    onSubmit: (data: SignUpForm) => void;
 }; 

export const Reg: React.FC<Props> = (props:Props) => {
    const {
      register,
      control,
      handleSubmit,
      reset,
      formState:{ isValid, isSubmitSuccessful } ,
      getValues
    } = useForm<SignUpForm>({defaultValues , mode:"all"} )

  const handleSignupSubmit = (e: FormEvent) => {
    /*...................*/ 
    e.preventDefault();
    reset();
  }

  const { register: verifyRegister, getValues: verifyGetValues, formState :{isSubmitted} } =
    useForm<VerifyForm>({
      defaultValues: {
        otp: '',
      },
    });

  const handleVerifySubmit = (e: FormEvent) => {
    /*...................*/ 
    e.preventDefault();
    reset();
  };
  
  return (
    <div className ={styles.back}>
    <form onSubmit={handleSignupSubmit}>
      <h2 className={styles.title}>Добро пожаловать</h2>
      <p>Найдите то, что Вам подходит</p>
      <Controller
        control={control}
        name="username"
        rules={{
          required: true,
          minLength: {
            value:6,
            message: "Имя пользователя дожно содержать как минимум 6 символов"
          },
          maxLength: {
            value: 20,
            message: "Имя пользователя не должно быть длиннее 20 символов"
          }}} 

        render={({ field: { onChange, value, name } }) => (
          <FormInput value={value} name={name} onChange={onChange} type="text" className={styles.items} placeholder='User123'/>
        )}
      />
      <Controller
        control={control}
        name="email"
        rules={{
          required: true,
          pattern: {
            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i ,
            message: "Введите действительный email"
          }
         }} 

        render={({ field: { onChange, value, name } }) => (
          <FormInput  value={value} name={name} onChange={onChange} type ="email" className={styles.items} placeholder='hello@gmail.com'/>
        )}
      />
      <Controller
        control={control}
        name="password"
        
        rules={{
          required: true,  
          pattern : {
            value:/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/i,
            message :"Минимум восемь символов, минимум одна буква, одна цифра и один специальный символ"
          }    
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput  value={value} name={name} onChange={onChange} type="password" className={styles.items} placeholder="" />
        )}
      />
      <Controller
        control={control}
        name="confirmPassword"
        rules={{
          required: true, 
          validate: (match) => {
            const password = getValues("password")
            return match === password || "Пароли должны соответсвовать"}    
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput value={value} name={name} onChange={onChange} type="password" className={styles.items} placeholder=''/>
        )}
      />
    <button type="submit" disabled={!isValid} className={styles.Submit}>Регистрация</button>
    <p>Есть аккаунт?<Link to ="/somtthing">Войдите здесь</Link></p>
    </form>
    <div>{isSubmitSuccessful && <form className="Verify__form" onSubmit={handleVerifySubmit}>
        <label>Код подтверждения<input type="text" {...verifyRegister('otp')} /></label>
        <button type="submit" disabled={!isValid} >Отправить код подтверждения</button>
      </form>}</div>
    <div>{isSubmitted && <Link to ="/MainPage">Домой</Link>}</div>  
    </div>
  );
};
